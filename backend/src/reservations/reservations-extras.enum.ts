export enum Extras {
    None = 'None',
    Booster = 'Booster Seat',
    ToddlerSeat = 'Toddler Seat',
    InfantSeat = 'Infant Seat'
}
